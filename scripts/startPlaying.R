# Start playing
# Mark Peterson
# 2015/Mar/17

# Big ideas:
# Outcome:
# - Win/loss for team A
# - Can/should I use regular season games (maybe only Neutral site) in training?
# Predictors should be log(ratio) of:
# - WP
# - WP last 10 games
# - RPI
# - Seed
# - Distance from home (set to minimum of 1 mile to avoid infinites if needed)
# - Matchups in season (plus low info prior)
# - Historical matchups (plus low info prior)
# - Record vs common opponents (plus low info prior; use PW method of sum WP's)

setwd("~/Documents/programStuff/R/kaggle/marchMadness2015/sampleScripts")

options(stringsAsFactors = FALSE)

regSeason <- read.csv("../data/regular_season_compact_results.csv"
                      , colClasses = c("character"
                                       , "numeric"
                                       , "character"
                                       , "numeric"
                                       , "character"
                                       , "numeric"
                                       , "factor"
                                       , "numeric"
                                       ))
head(regSeason)
summary(regSeason)

thisSeason <- read.csv("../data/regular_season_compact_results_2015.csv"
                       , colClasses = c("character"
                                        , "numeric"
                                        , "character"
                                        , "numeric"
                                        , "character"
                                        , "numeric"
                                        , "factor"
                                        , "numeric"
                       ))

head(thisSeason)


tourney <- read.csv("../data/tourney_compact_results.csv"
                    , colClasses = c("character"
                                     , "numeric"
                                     , "character"
                                     , "numeric"
                                     , "character"
                                     , "numeric"
                                     , "factor"
                                     , "numeric"
                    ))
head(tourney)
summary(tourney)
nrow(tourney)

thisYearTourney <- read.csv("../data/sample_submission_2015.csv"
                            , colClasses = c(
                              
                              ))
head(thisYearTourney)
toPredict <- data.frame(do.call(rbind,strsplit(thisYearTourney$id,"_")))
names(toPredict) <- c("season","teamA","teamB")
head(toPredict)

slots <- read.csv("../data/tourney_slots_and_geog.csv")
head(slots)
tail(slots)

splitSeasons <- split(regSeason, regSeason$season)
splitSeasons[["2015"]] <- thisSeason

# thisSeason <- splitSeasons[[1]]
rpiScale <- c(  H  = 0.6
              , N  = 1
              , A  = 1.4
              )

teamData <- lapply(splitSeasons,function(thisSeason){
  teams <- sort(unique(c(thisSeason$wteam, thisSeason$lteam)))
  allTeams <- c(thisSeason$wteam, thisSeason$lteam)
  
  temp <- data.frame(row.names = teams)
  
  temp$wp <- unlist(lapply(teams,function(thisTeam){
    sum(thisSeason$wteam == thisTeam) / sum(allTeams == thisTeam)
  }))
  # hist(temp$wp)
  # mean(temp$wp)
  
  temp$wpLastTen <- unlist(lapply(teams,function(thisTeam){
    whichWins <- which(thisSeason$wteam == thisTeam)
    whichLoss <- which(thisSeason$lteam == thisTeam)
    lastGames <- tail(sort(c(whichWins, whichLoss)),10)
    
    out <- sum(thisSeason$wteam[lastGames] == thisTeam) / sum(allTeams[c(lastGames, lastGames+nrow(thisSeason))] == thisTeam)
    return(out)
  }))
  #hist(temp$wpLastTen)
  #mean(temp$wpLastTen)
  
  # thisTeam <- teams[[1]]
  temp$wpForRPI <- unlist(lapply(teams,function(thisTeam){
    whichWins <- which(thisSeason$wteam == thisTeam)
    whichLoss <- which(thisSeason$lteam == thisTeam)
    wins <- sum(rpiScale[thisSeason$wloc[whichWins]])
    loss <- sum(rpiScale[thisSeason$wloc[whichLoss]])
    return(wins/(wins+loss))
  }))
  #hist(temp$wpForRPI)
  #plot(wpForRPI ~ wp, data = temp)
  
  temp$owpForRPI <- unlist(lapply(teams,function(thisTeam){
    otherTeams <- unique(c( thisSeason$wteam[thisSeason$lteam == thisTeam]
                           ,thisSeason$lteam[thisSeason$wteam == thisTeam]))
    
    # x <- otherTeams[[1]]
    tempOWP <- unlist(lapply(otherTeams,function(x){
      wins <- sum(thisSeason$wteam[thisSeason$lteam != thisTeam] == x)
      loss <- sum(thisSeason$lteam[thisSeason$wteam != thisTeam] == x)
      return(wins/(wins+loss))
    }))
    return(mean(tempOWP))
  }))
  
  temp$oowpForRPI <- unlist(lapply(teams,function(thisTeam){
    otherTeams <- unique(c( thisSeason$wteam[thisSeason$lteam == thisTeam]
                            ,thisSeason$lteam[thisSeason$wteam == thisTeam]))
    out <- mean(temp[as.character(otherTeams),"owpForRPI"])
    return(out)
  }))
  
  temp$RPI <- 0.25 * temp$wpForRPI + 0.5 * temp$owpForRPI + 0.25 * temp$oowpForRPI
  # hist(temp$RPI)
  # plot(RPI ~ wp, data = temp)

  
  return(temp)
  
  })

lapply(teamData,head)


head(tourney)

# Randomize so that not all winners are first
whichFirst <- sample(c("A","B")
                     , nrow(tourney)
                     , TRUE)
toBuild <- data.frame(season = tourney$season
                      , winner = whichFirst
                      , teamA = NA
                      , teamB = NA
                      , row.names = 1:nrow(tourney))
toBuild$winner <- factor(toBuild$winner)
toBuild$teamA[whichFirst == "A"] <- tourney$wteam[whichFirst == "A"]
toBuild$teamA[whichFirst != "A"] <- tourney$lteam[whichFirst != "A"]
toBuild$teamB[whichFirst != "A"] <- tourney$wteam[whichFirst != "A"]
toBuild$teamB[whichFirst == "A"] <- tourney$lteam[whichFirst == "A"]
head(toBuild)

# x <- (toBuild[1,])
toBuild$wp <-
  apply(toBuild,1,function(x){
    log2(teamData[[x[["season"]] ]][x[["teamA"]],"wp"] / teamData[[x[["season"]] ]][x[["teamB"]],"wp"])
  })
#hist(toBuild$wp)

toBuild$wpLastTen <-
  apply(toBuild,1,function(x){
    log2(teamData[[x[["season"]] ]][x[["teamA"]],"wpLastTen"] / teamData[[x[["season"]] ]][x[["teamB"]],"wpLastTen"])
  })

toBuild$RPI <-
  apply(toBuild,1,function(x){
    log2(teamData[[x[["season"]] ]][x[["teamA"]],"RPI"] / teamData[[x[["season"]] ]][x[["teamB"]],"RPI"])
  })
# hist(toBuild$RPI)

toBuild$headToHead <- 
  apply(toBuild,1,function(x){
    thisSeasonData <- splitSeasons[[x[["season"]]]]
    hthW <- sum( thisSeasonData$wteam == x[["teamA"]] & thisSeasonData$lteam == x[["teamB"]])
    hthL <- sum( thisSeasonData$wteam == x[["teamB"]] & thisSeasonData$lteam == x[["teamA"]])
    return(log2( (hthW+1) / (hthL+1)))
  })
# hist(toBuild$headToHead)

toBuild$histHtH <- 
  apply(toBuild,1,function(x){
    hthW <- sum( regSeason$wteam == x[["teamA"]] & regSeason$lteam == x[["teamB"]]) +
            sum(   tourney$wteam == x[["teamA"]] &   tourney$lteam == x[["teamB"]] &
                     as.numeric(tourney$season) < as.numeric(x[["season"]]) )
    hthL <- sum( regSeason$wteam == x[["teamB"]] & regSeason$lteam == x[["teamA"]]) +
            sum(   tourney$wteam == x[["teamB"]] &   tourney$lteam == x[["teamA"]] &
                     as.numeric(tourney$season) < as.numeric(x[["season"]]) )
    return(log2( (hthW+1) / (hthL+1)))
  })
hist(toBuild$histHtH)


head(toBuild)

save(teamData, file = "../analysis/teamData.RData")
save(toBuild, file = "../analysis/toBuild.RData")

load("../analysis/teamData.RData")
load("../analysis/toBuild.RData")

toBuild$winner <- factor(toBuild$winner, levels = c("B","A"))

baseModel <- glm(winner ~ wp + wpLastTen + RPI
                 , data = toBuild
                 , family = binomial("logit"))
summary(baseModel)
anova(baseModel)

lowModel <-
  glm(winner ~ RPI
      , data = toBuild
      , family = binomial("logit"))
summary(lowModel)
guesses <- predict(lowModel, type = "response")
hist(guesses)

howClose <- table(guesses > 0.5, toBuild$winner)
addmargins(howClose)
mosaicplot(howClose)
boxplot(guesses ~ toBuild$winner)
plot(residuals(lowModel) ~ toBuild$RPI)
hist(toBuild$RPI)
plot(guesses ~ toBuild$RPI)
points(toBuild$RPI[toBuild$winner == "A"], guesses[toBuild$winner == "A"]
       , col = "red3")
offsets <- c(A = .03, B = -0.03)
plot(guesses+offsets[toBuild$winner] ~ toBuild$RPI)
isCorrect <- 
  (guesses < 0.5 & toBuild$winner == "B") |
  (guesses > 0.5 & toBuild$winner == "A")
mean(isCorrect)

-mean( isCorrect*log(guesses) + !isCorrect*log(1-guesses))


# Cross validation
withH2H <-
  glm(winner ~ RPI + histHtH
      , data = toBuild
      , family = binomial("logit"))
summary(withH2H)
guesses <- predict(withH2H, type = "response")
hist(guesses)

howClose <- table(guesses > 0.5, toBuild$winner)
addmargins(howClose)
mosaicplot(howClose)
boxplot(guesses ~ toBuild$winner)
offsets <- c(A = .03, B = -0.03)
plot(guesses+offsets[toBuild$winner] ~ toBuild$RPI)
isCorrect <- 
  (guesses < 0.5 & toBuild$winner == "B") |
  (guesses > 0.5 & toBuild$winner == "A")
mean(isCorrect)

-mean( isCorrect*log(guesses) + !isCorrect*log(1-guesses))




# Quick do this season

toPredict$wp <-
  apply(toPredict,1,function(x){
    log2(teamData[[x[["season"]] ]][x[["teamA"]],"wp"] / teamData[[x[["season"]] ]][x[["teamB"]],"wp"])
  })
#hist(toBuild$wp)

toPredict$wpLastTen <-
  apply(toPredict,1,function(x){
    log2(teamData[[x[["season"]] ]][x[["teamA"]],"wpLastTen"] / teamData[[x[["season"]] ]][x[["teamB"]],"wpLastTen"])
  })

toPredict$RPI <-
  apply(toPredict,1,function(x){
    log2(teamData[[x[["season"]] ]][x[["teamA"]],"RPI"] / teamData[[x[["season"]] ]][x[["teamB"]],"RPI"])
  })
# hist(toBuild$RPI)

head(toPredict)

predictions <- predict(lowModel
                       , type = "response"
                       , newdata = toPredict)
hist(predictions)

out <- data.frame(id = paste(toPredict$season
                             , toPredict$teamA
                             , toPredict$teamB
                             , sep= "_")
                  , pred = predictions)
write.csv(out, "../analysis/veryVeryBasicModel.csv"
          , row.names = FALSE
          , quote = FALSE)
